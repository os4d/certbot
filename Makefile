VERSION?=dev
BUILD_DATE:=$(shell date +"%Y-%m-%d %H:%M")
CMD?=get
.PHONY: help runlocal i18n

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z0-9_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT

help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)


build:  ## build
	@echo "Building ${CI_REGISTRY_IMAGE}:${VERSION}"
	@-@docker pull ${CI_REGISTRY_IMAGE}:latest

	@docker build \
		--cache-from ${CI_REGISTRY_IMAGE}:latest \
		--build-arg VERSION=${VERSION} \
		--build-arg BUILD_DATE="${BUILD_DATE}" \
		-t ${CI_REGISTRY_IMAGE}:${VERSION} \
		-f Dockerfile .


info:  # retrieve image info
	 docker run --rm --tty --entrypoint /bin/bash -t ${CI_REGISTRY_IMAGE}:${VERSION} info.sh


release:
	docker tag ${CI_REGISTRY_IMAGE}:${VERSION} ${CI_REGISTRY_IMAGE}:latest
	docker push ${CI_REGISTRY_IMAGE}:${VERSION}
	docker push ${CI_REGISTRY_IMAGE}:latest


shell:  ## shell
	EXTRA="" \
	CMD="/bin/bash" \
 	$(MAKE) run


dev:
	CMD="/bin/bash" \
    EXTRA="-v ${PWD}/tests:/tests \
    -v ${PWD}/bin/entrypoint.sh:/usr/local/bin/entrypoint.sh \
    -v ${PWD}/bin/certbot.sh:/usr/local/bin/certbot.sh \
    -v ${PWD}/bin/go_daddy_auth.sh:/usr/local/bin/go_daddy_auth.sh \
    -v ${PWD}/bin/info.sh:/usr/local/bin/info.sh " \
 	$(MAKE) run


run:
	mkdir -p ${PWD}/~work/logs ${PWD}/~work/certs
	docker run --rm -it \
		${EXTRA} \
		-v ${PWD}/~work/:/etc/letsencrypt/ \
		-v ${PWD}/~work/logs:/logs/ \
  	    -e CERTBOT_EMAIL="${CERTBOT_EMAIL}" \
  	    -e CERTBOT_DOMAIN="${CERTBOT_DOMAIN}" \
  	    -e DADDY_AUTH_KEY="${DADDY_AUTH_KEY}" \
	    -e DADDY_AUTH_SECRET="${DADDY_AUTH_SECRET}" \
	    -e DADDY_SERVER="${DADDY_SERVER}" \
		-t ${CI_REGISTRY_IMAGE}:${VERSION} \
		${CMD}

test:
	@CMD="/tests/run_tests.sh" \
	EXTRA="-v ${PWD}/tests:/tests"  \
	$(MAKE) run

