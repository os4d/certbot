FROM certbot/certbot
ARG BUILD_DATE
ARG VERSION

RUN apk add bind-tools bash curl

ADD ./bin/* /usr/local/bin/

ENV PATH="${PATH}:/root/.local/bin:/usr/local/bin/" \
    VERSION=${VERSION} \
    BUILD_DATE=${BUILD_DATE} \
    CERTBOT_EMAIL="admin@example.com" \
    CERTBOT_DOMAIN="example.com" \
    DADDY_AUTH_KEY="" \
    DADDY_AUTH_SECRET="" \
    DADDY_SERVER="https://api.godaddy.com"

RUN mkdir /logs /certs

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD "get"
