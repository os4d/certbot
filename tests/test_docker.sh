
WORKDIR=${PWD}/~work/

rm -fr ${WORKDIR}
mkdir -p ${WORKDIR}/{etc/letsencrypt,var/lib/letsencrypt} ${WORKDIR}/logs
export LOG_DIR=${WORKDIR}/logs

docker run --pull always -it --rm --name certbot \
    -v ${PWD}/bin/entrypoint.sh:/usr/local/bin/entrypoint.sh \
    -v ${PWD}/bin/certbot.sh:/usr/local/bin/certbot.sh \
    -v ${PWD}/bin/go_daddy_auth.sh:/usr/local/bin/go_daddy_auth.sh \
    -v ${PWD}/bin/info.sh:/usr/local/bin/info.sh \
    -v ${WORKDIR}/logs/:/logs \
    -v ${WORKDIR}/etc/letsencrypt/:/etc/letsencrypt \
    -v ${WORKDIR}/var/lib/letsencrypt/:/var/lib/letsencrypt \
    -e DADDY_AUTH_KEY=$DADDY_KEY \
    -e DADDY_AUTH_SECRET=$DADDY_SECRET \
    -e DADDY_SERVER=$DADDY_SERVER \
    -e CERTBOT_DOMAIN=lt.alertel.it \
    -e CERTBOT_EMAIL=s.apostolico@gmail.com \
    registry.gitlab.com/os4d/certbot get

if [ -f ${WORKDIR}/aaaaa ];then
fi