set -e

WORKDIR=${PWD}/~work/

rm -fr ${WORKDIR}
mkdir -p ${WORKDIR}/{etc/letsencrypt,var/lib/letsencrypt} ${WORKDIR}/logs

TODAY="$(date +%Y-%m-%d)"
NOW="$(date +%Y-%m-%d.%H:%M:%S)"
TIMESTAMP="$(date +%Y%m%d%H%M%S)"

export LOG_DIR=${WORKDIR}/logs
export TODAY TIMESTAMP NOW
export CERTBOT_VALIDATION="validation-${NOW}"
export CERTBOT_TOKEN="token-${NOW}"
#
export DELAY_AFTER_DNS_RECORD_UPDATE=30
export DADDY_SECRET=10
#
go_daddy_auth.sh
#
#CHECK=$(dig +short _acme-challenge.lt.sosbob.com txt | sed 's/"//g')
#if [ $CHECK != $CERTBOT_VALIDATION ]; then
#  echo "ERROR: Found '$CHECK', Expected: '$CERTBOT_VALIDATION'"
#  exit 1
#fi
#
##
##if [ -f $ACME_CHALLENGE_DIR/$CERTBOT_VALIDATION ];then
##  exit 0
##else
##  echo "ERROR: $ACME_CHALLENGE_DIR/$CERTBOT_VALIDATION Not found"
##  exit 1
##fi