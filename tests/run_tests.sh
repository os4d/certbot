SAVED_PATH=${PATH}
function on_exit() {
  export PATH=${SAVED_PATH}
}
trap on_exit EXIT


ME=$(readlink -f ${BASH_SOURCE[0]})
TEST_DIR=$(dirname ${ME})

echo "script: ${ME}"
echo "test dir: ${TEST_DIR}"
echo

. ${TEST_DIR}/lib.sh

for file in $(ls ${TEST_DIR}/test_*); do
  echo -n "$file ..."
  OUT=$(sh $file)
  if [[ $? = 0 ]];then
    echo "OK"
  else
    echo "FAIL"
  fi
done
