# certbot-godaddy

Simple [GoDaddy](https://www.godaddy.com/) enabled [certbot](https://certbot.eff.org/) docker image.

Based on official [certbot](https://hub.docker.com/r/certbot/certbot) image add a go-daddy script.

How to use it:
-------------
### Get new certificate

1. Get your GoDaddy API credentials at https://developer.godaddy.com/keys

2. Run the following command

        sudo docker run -it --rm --name certbot \
                -v "/etc/letsencrypt:/etc/letsencrypt" \
                -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \
                -e DADDY_AUTH_KEY="<API_KEY>" \
                -e DADDY_AUTH_SECRET="<API_SECRET>" \
                -e CERTBOT_DOMAIN=example.com \
                -e CERTBOT_EMAIL=admin@example.com \
                os4d/certbot 

    ----
    > **_NOTE:_** this will automatically run the following command:
    
    > $`certbot certonly --manual --manual-auth-hook "/usr/local/bin/go_daddy_auth.sh" --domain example.com 
    --email admin@example.com --no-eff-email --agree-tos --noninteractive --force-renew --preferred-challenges dns --logs-dir /logs `
    ----

### Renew certificate

1Run the following command

        sudo docker run -it --rm --name certbot \
                -v "/etc/letsencrypt:/etc/letsencrypt" \
                -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \
                -e CERTBOT_DOMAIN=example.com \
                os4d/certbot renew 

## How to

### Pass extra args to certbot

       sudo docker run -it --rm --name certbot \
                -v "/etc/letsencrypt:/etc/letsencrypt" \
                -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \
                -e CERTBOT_DOMAIN=example.com \
                os4d/certbot \
                get --dry-run

### Cutomize low level certbot command

       sudo docker run -it --rm --name certbot \
                -v "/etc/letsencrypt:/etc/letsencrypt" \
                -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \
                -e CERTBOT_DOMAIN=example.com \
                os4d/certbot \
                certbot ...


> **_NOTE:_** If you are getting new certificate, do not forget to add the auth hook
>
>`--manual --manual-auth-hook "/usr/local/bin/go_daddy_auth.sh"`



### Reload NGINX on renew

       sudo docker run -it --rm --name certbot \
                -v "/etc/letsencrypt:/etc/letsencrypt" \
                -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \
                -e CERTBOT_DOMAIN=example.com \
                os4d/certbot renew \
                --post-hook "service nginx reload"