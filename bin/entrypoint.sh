#!/bin/bash

echo "COMMAND: $@"

case "$1" in
"get")
  shift
  EMAIL=""
  STAR=""
  if [ -n "$CERTBOT_EMAIL" ];then
    EMAIL=" --email ${CERTBOT_EMAIL}"
  fi

  if [ -n "$STAR_CERTIFICATE" ]; then
    STAR=" --domain *.${CERTBOT_DOMAIN}"
  fi

  echo "CERTBOT_DOMAIN   ${CERTBOT_DOMAIN}"
  echo "EMAIL            ${EMAIL}"
  echo "STAR_CERTIFICATE ${STAR}"
  export STAR_CERTIFICATE

  certbot \
    certonly \
    --manual \
    --manual-auth-hook "/usr/local/bin/go_daddy_auth.sh" \
    --domain ${CERTBOT_DOMAIN} \
    ${STAR} \
    ${EMAIL} \
    --no-eff-email \
    --agree-tos \
    --noninteractive \
    --force-renew \
    --preferred-challenges dns \
    --logs-dir /logs \
    $@
  ;;
"renew")
  shift
  certbot renew $@
  ;;
"shell")
  exec "/bin/bash"
  ;;
*)
  exec "$@"
  ;;
esac
