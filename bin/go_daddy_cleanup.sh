#!/bin/bash


DELAY_AFTER_DNS_RECORD_UPDATE=${DELAY_AFTER_DNS_RECORD_UPDATE:-30}
# Time interval between each dig request (in seconds)
DIG_TIME_INTERVAL=${DIG_TIME_INTERVAL:-15}
# Number of retries of dig request before ending in a failure
DIG_NB_RETRIES=25
LOG_DIR="/logs"
LOG_FILE="$LOG_DIR/cert.$CERTBOT_DOMAIN.log"

if [ -z $DADDY_AUTH_SECRET ]; then
  echo "Set DADDY_AUTH_SECRET env var"
  exit 1
fi
if [ -z $DADDY_AUTH_KEY ]; then
  echo "Set DADDY_AUTH_KEY env var"
  exit 1
fi


if [ -f ${LOG_FILE} ]
then
  echo -n "" > ${LOG_FILE}
else
  touch ${LOG_FILE}

fi
pad () { [ "$#" -gt 1 ] && [ -n "$2" ] && printf "%$2.${2#-}s" "$1"; }

dump() {
  line="$(pad "$1" -20)  ${!1}"
  log "$line"
}

log() {
  DATE=$(date)
  echo "$DATE: $1" >> $LOG_FILE
  echo "$1"
}
DADDY_SERVER=${DADDY_SERVER:-https://api.godaddy.com}
dump "LOG_FILE"
dump "DADDY_SERVER"
dump "DADDY_AUTH_KEY"
dump "DADDY_AUTH_SECRET"
dump "CERTBOT_DOMAIN"
dump "CERTBOT_VALIDATION"
dump "CERTBOT_TOKEN"
dump "CERTBOT_ALL_DOMAINS"
dump "CERTBOT_REMAINING_CHALLENGES"
dump "CERTBOT_AUTH_OUTPUT"


DOMAIN=$(expr "$CERTBOT_DOMAIN" : '.*\.\(.*\..*\)')
if [ -z ${DOMAIN} ];then
  DOMAIN="$CERTBOT_DOMAIN"
  SUBDOMAIN=""
  CHECK_NAME="_acme-challenge.$CERTBOT_DOMAIN"
  RECORD_NAME="_acme-challenge"
else
  SUBDOMAIN=$(echo "$CERTBOT_DOMAIN" | sed "s/.$DOMAIN//")
  CHECK_NAME="_acme-challenge.$CERTBOT_DOMAIN"
  RECORD_NAME="_acme-challenge.$SUBDOMAIN"
fi

dump "DOMAIN"
dump "SUBDOMAIN"

#RECORD_NAME_ALT="_acme-challenge.*.$SUBDOMAIN"
RECORD_VALUE="none"
RECORD_TYPE=TXT
RESPONSE_CODE=""
dump "CHECK_NAME"
dump "RECORD_NAME"
dump "RECORD_NAME_ALT"
dump "RECORD_VALUE"

if  [ -n "$CERTBOT_AUTH_OUTPUT" ]; then
  log "cleanup"
  exit 0
fi

NS=$(echo $(dig +short $DOMAIN ns) | awk '{print $1}')
dump "NS"

get_current_value(){
  echo "$(dig +short @$NS $1 txt | sed 's/"//g')"
}

set_or_create() {
  value="$1.$DOMAIN"
  log "Checking '$value'"
#  CURRENT_VALUE=$(dig +short @$NS $value txt)
  CURRENT_VALUE=$(get_current_value $value)
  log "Current Value: $CURRENT_VALUE"
  if [[ -n $CURRENT_VALUE ]]; then
    log "Updating $1 to $RECORD_VALUE"
    curl -s -X PUT -w %{http_code} \
    "$DADDY_SERVER/v1/domains/$DOMAIN/records/$RECORD_TYPE/$1" \
    -H "Authorization: sso-key ${DADDY_AUTH_KEY}:${DADDY_AUTH_SECRET}" \
    -H "Content-Type: application/json" \
    -d "[{\"data\": \"$RECORD_VALUE\", \"ttl\": 600}]"

    RESPONSE_CODE=$(curl -s -X PUT -w %{http_code} \
    "$DADDY_SERVER/v1/domains/$DOMAIN/records/$RECORD_TYPE/$1" \
    -H "Authorization: sso-key ${DADDY_AUTH_KEY}:${DADDY_AUTH_SECRET}" \
    -H "Content-Type: application/json" \
    -d "[{\"data\": \"$RECORD_VALUE\", \"ttl\": 600}]")
  else
    log "Adding $1"
    RESPONSE_CODE=$(curl -s -X PATCH -w %{http_code} \
    "$DADDY_SERVER/v1/domains/$DOMAIN/records" \
    -H "Authorization: sso-key ${DADDY_AUTH_KEY}:${DADDY_AUTH_SECRET}" \
    -H "Content-Type: application/json" \
    -d "[{\"data\": \"${RECORD_VALUE}\", \
        \"name\": \"${value}\", \
        \"priority\": 10, \
        \"ttl\": 600, \
        \"type\": \"TXT\"}]")
  fi
}

set_or_create $RECORD_NAME
echo ">>>>$RESPONSE_CODE"

if [ "$RESPONSE_CODE" != "200" ]; then
  log "Failure $RESPONSE_CODE"
  exit 1
fi

log "sleep for $DELAY_AFTER_DNS_RECORD_UPDATE"
sleep $DELAY_AFTER_DNS_RECORD_UPDATE
I=0
while [ $I -le $DIG_NB_RETRIES ]
do
  sleep $DIG_TIME_INTERVAL
  log "dig +short $CHECK_NAME txt | grep -e \"$RECORD_VALUE\""
  R=$(get_current_value $CHECK_NAME)
  if [ $R = $RECORD_VALUE ]; then
    log "Authoritative updated"
    break;
  else
    log "Authoritative pending updates"
    I=$((I+1))
  fi
done


log "Wait DNS propagation"
I=0
while [ $I -le $DIG_NB_RETRIES ]
do
  sleep $DIG_TIME_INTERVAL
  log "dig +short $CHECK_NAME txt | grep -e \"$RECORD_VALUE\""
  R=$(dig +short $CHECK_NAME txt | sed 's/"//g')
  if [ $R = $RECORD_VALUE ]; then
    log "SUCCESS '$I' > TOKEN FOUND: '${R}'";
    break;
  else
    log "FAIL '$I' > TOKEN NOT FOUND - '${R}'";
    I=$((I+1))
  fi
done