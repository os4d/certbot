#!/bin/sh

echo "
- OS4D certbot-godaddy '${VERSION}' build '${BUILD_DATE}'
"
bash --version | grep  -o '.*version [0-9]*\.[0-9]*\.[0-9]*'
certbot --version
curl --version | grep 'curl'
