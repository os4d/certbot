#!/bin/bash

EMAIL=""
if [ -n "$CERTBOT_EMAIL" ];then
  EMAIL="--email ${CERTBOT_EMAIL}"
fi

certbot \
    certonly \
    --manual \
    --manual-auth-hook "/usr/local/bin/go_daddy_auth.sh" \
    --manual-cleanup-hook  "/usr/local/bin/go_daddy_cleanup.sh" \
    --domain ${CERTBOT_DOMAIN} \
    --domain "*.${CERTBOT_DOMAIN}" \
    ${EMAIL} \
    --no-eff-email \
    --agree-tos \
    --noninteractive \
    --force-renew \
    --preferred-challenges dns \
    --logs-dir /logs \
    $@